﻿using System;

namespace LINQ2Transaction
{
    class Program
    {
        static void Main(string[] args)
        {
            Transaction tfs1 = new Transaction();
            tfs1.TransactionId = 1;
            tfs1.DepositAmount = 10.00m;
            tfs1.PresenterId = 154;
            tfs1.LocationName = "Bankshot Location 1";
            tfs1.ItemCount = 1;

            Transaction tfs2 = new Transaction();
            tfs2.TransactionId = 2;
            tfs2.DepositAmount = 20.00m;
            tfs2.PresenterId = 100;
            tfs2.LocationName = "GB Location 1";
            tfs2.ItemCount = 2;

            Transaction tfs3 = new Transaction();
            tfs3.TransactionId = 2;
            tfs3.DepositAmount = 30.00m;
            tfs3.PresenterId = 105;
            tfs3.LocationName = "BMobile Location";
            tfs3.ItemCount = 3;

            Transaction tfs4 = new Transaction();
            tfs4.TransactionId = 2;
            tfs4.DepositAmount = 40.00m;
            tfs4.PresenterId = 107;
            tfs4.LocationName = "Planet Location 1";
            tfs4.ItemCount = 4;

            Transaction tfs5 = new Transaction();
            tfs5.TransactionId = 2;
            tfs5.DepositAmount = 50.00m;
            tfs5.PresenterId = 109;
            tfs5.LocationName = "Planet Location 2";
            tfs5.ItemCount = 5;

            Transaction[] firstTransactionsSet = new Transaction[] { tfs1, tfs2, tfs3, tfs4, tfs5 };

            Transaction tss1 = new Transaction();
            tss1.TransactionId = 4;
            tss1.DepositAmount = 40.00m;
            tss1.PresenterId = 107;
            tss1.LocationName = "Planet Location 1";
            tss1.ItemCount = 1;

            Transaction tss2 = new Transaction();
            tss2.TransactionId = 5;
            tss2.DepositAmount = 50.00m;
            tss2.PresenterId = 109;
            tss2.LocationName = "Planet Location 2";
            tss2.ItemCount = 1;

            Transaction tss3 = new Transaction();
            tss3.TransactionId = 7;
            tss3.DepositAmount = 30.00m;
            tss3.PresenterId = 71;
            tss3.LocationName = "Cloud Location 1";
            tss3.ItemCount = 1;

            Transaction tss4 = new Transaction();
            tss4.TransactionId = 3;
            tss4.DepositAmount = 17.00m;
            tss4.PresenterId = 85;
            tss4.LocationName = "Green Location 3";
            tss4.ItemCount = 5;

            Transaction tss5 = new Transaction();
            tss5.TransactionId = 4;
            tss5.DepositAmount = 8.00m;
            tss5.PresenterId = 73;
            tss5.LocationName = "Sky Location 2";
            tss5.ItemCount = 1;

            decimal testAmount = 15.00m;

            Transaction[] secondTransactionsSet = new Transaction[] { tss1, tss2, tss3, tss4, tss5 };
            Transaction[] transactionsSet = null;

            Transaction[] exceptResult = ExceptByPresenterId(firstTransactionsSet, secondTransactionsSet);
            Transaction[] unionResalt = Union(firstTransactionsSet, secondTransactionsSet);
            Transaction first = First(unionResalt);
            Transaction last = Last(unionResalt);
            Transaction[] intersectResult = IntersectByPresenterId(firstTransactionsSet, secondTransactionsSet);
            Transaction[] whereDepositAmountLessThanResult = WhereDepositAmountLessThan(firstTransactionsSet, testAmount);
            Transaction[] whereDepositAmountGreaterThanResult = WhereDepositAmountGreaterThan(firstTransactionsSet, testAmount);
            bool everyHasSameItemCountResult = EveryHasSameItemCount(secondTransactionsSet);
            Transaction[] distinctByTransactionIdResult = DistinctByTransactionId(firstTransactionsSet);
            Transaction[] overloadedDistinctByTransactionIdResult = DistinctByTransactionId(firstTransactionsSet, secondTransactionsSet);
            Console.ReadKey();
        }

        static Transaction[] ExceptByPresenterId(Transaction[] trSet1, Transaction[] trSet2)
        {
            int resultLength = 0;
            Transaction[] result = null;
            for (int i = 0; i < trSet1.Length; i++)
            {
                Transaction trSet1Item = trSet1[i];
                bool needIncrementResultLength = false;
                for (int j = 0; j < trSet2.Length; j++)
                {
                    Transaction trSet2Item = trSet2[j];
                    if (trSet1Item.PresenterId != trSet2Item.PresenterId)
                    {
                        needIncrementResultLength = true;
                    }
                    else
                    {
                        needIncrementResultLength = false;
                        break;
                    }
                }
                if (needIncrementResultLength)
                {
                    resultLength++;
                }
            }
            result = new Transaction[resultLength];

            int resultIndex = 0;
            for (int k = 0; k < trSet1.Length; k++)
            {
                Transaction trSet1Item = trSet1[k];
                bool needAddCurentItem = false;
                for (int j = 0; j < trSet2.Length; j++)
                {
                    Transaction trSet2Item = trSet2[j];

                    if (trSet1Item.PresenterId != trSet2Item.PresenterId)
                    {
                        needAddCurentItem = true;
                    }
                    else
                    {
                        needAddCurentItem = false;
                        break;
                    }


                }
                if (needAddCurentItem)
                {
                    result[resultIndex++] = trSet1Item;
                }

            }


            return result;
        }

        static Transaction[] IntersectByPresenterId(Transaction[] trSet1, Transaction[] trSet2)
        {
            int resultLength = 0;
            Transaction[] result = null;
            for (int i = 0; i < trSet1.Length; i++)
            {
                Transaction set1Item = trSet1[i];
                for (int j = 0; j < trSet2.Length; j++)
                {
                    Transaction set2Item = trSet2[j];
                    if (set1Item.PresenterId == set2Item.PresenterId)
                    {
                        resultLength++;
                        break;
                    }
                }
            }
            result = new Transaction[resultLength];
            int resultIndex = 0;
            for (int i = 0; i < trSet1.Length; i++)
            {
                Transaction set1Item = trSet1[i];
                for (int j = 0; j < trSet2.Length; j++)
                {
                    Transaction set2Item = trSet2[j];
                    if (set1Item.PresenterId == set2Item.PresenterId)
                    {
                        result[resultIndex++] = set1Item;
                        break;
                    }
                }
            }
            return result;
        }

        static Transaction[] Union(Transaction[] trSet1, Transaction[] trSet2)
        {
            Transaction[] result = new Transaction[trSet1.Length + trSet2.Length];
            Array.Copy(trSet1, result, trSet1.Length);
            Array.Copy(trSet2, 0, result, trSet1.Length, trSet2.Length);

            return result;
        }

        static Transaction First(Transaction[] trSet)
        {
            Transaction result = null;
            if (trSet != null && trSet.Length > 0)
            {
                result = trSet[0];
            }
            return result;
        }

        static Transaction Last(Transaction[] trSet)
        {

            Transaction result = null;
            if (trSet != null && trSet.Length > 0)
            {
                result = trSet[trSet.Length - 1];
            }

            return result;
        }

        static bool EveryHasSameItemCount(Transaction[] trSet)
        {
            bool result = false;
            Transaction firstItem = First(trSet);
            for (int i = 1; i < trSet.Length; i++)
            {
                Transaction nextItem = trSet[i];
                if(firstItem.ItemCount == nextItem.ItemCount)
                {
                    result = true;
                }
                else
                {
                    result = false;
                    break;
                }
            }

            return result;
        }

        static Transaction[] WhereDepositAmountLessThan(Transaction[] trSet, decimal amount)
        {
            Transaction[] result = null;
            int resultLength = 0;
            for (int k = 0; k < trSet.Length; k++)
            {
                Transaction trSetItem = trSet[k];
                if (trSetItem.DepositAmount < amount)
                {
                    resultLength++;
                }
            }

            result = new Transaction[resultLength];

            for (int i = 0; i < trSet.Length; i++)
            {
                int index = 0;
                Transaction trSetItem = trSet[i];
                if (trSetItem.DepositAmount < amount)
                {
                    result[index++] = trSetItem;
                }           
            }

            return result;
        }

        static Transaction[] WhereDepositAmountGreaterThan(Transaction[] trSet, decimal amount)
        {
            Transaction[] result = null;
            int resultLength = 0;
            for (int i = 0; i < trSet.Length; i++)
            {
                Transaction trSetItem = trSet[i];
                if(trSetItem.DepositAmount > amount)
                {
                    resultLength++;
                }
            }
            result = new Transaction[resultLength];

            int index = 0;
            for (int k = 0; k < trSet.Length; k++)
            {
                Transaction trSetItem = trSet[k];
                if(trSetItem.DepositAmount > amount)
                {
                    result[index++] = trSetItem;
                }
            }
            return result;
        }
        static Transaction[] DistinctByTransactionId(Transaction[] trSet)
        {
            Transaction[] result = new Transaction[] { };

            for (int i = 0; i < trSet.Length; i++)
            {
                Transaction item = trSet[i];
                if (result.Length == 0)
                {
                    result = new Transaction[1] {item};
                }
                else
                {
                    bool contaisSameItem = false;
                    for (int k = 0; k < result.Length; k++)
                    {
                        Transaction resultItem = result[k];
                        if (resultItem.TransactionId == item.TransactionId)
                        {
                            contaisSameItem = true;
                            break;
                        }
                    }
                    if (!contaisSameItem)
                    {
                        Transaction[] newResult = new Transaction[result.Length + 1];
                        Array.Copy(result, 0, newResult, 0, result.Length);
                        newResult[newResult.Length - 1] = item;
                        result = newResult;
                    }

                }
                
            }
            return result;
        }
        static Transaction[] DistinctByTransactionId(Transaction[] trSet1, Transaction[] trSet2)
        {
            Transaction[] concotenatedArray = new Transaction[trSet1.Length + trSet2.Length];
            Array.Copy(trSet1, 0, concotenatedArray, 0, trSet1.Length);
            Array.Copy(trSet2, 0, concotenatedArray, trSet1.Length, trSet2.Length);
            Transaction[] result = new Transaction[] { };
            for (int i = 0; i < concotenatedArray.Length; i++)
            {
                Transaction item = concotenatedArray[i];
                if (result.Length == 0)
                {
                    result = new Transaction[1] { item };
                }
                else
                {
                    bool containsSameItem = false;
                    for (int j = 0; j < result.Length; j++)
                    {
                        Transaction resultItem = result[j];
                        if (resultItem.TransactionId == item.TransactionId)
                        {
                            containsSameItem = true;
                            break;
                        }
                    }
                    if (!containsSameItem)
                    {
                        Transaction[] newResult = new Transaction[result.Length + 1];
                        Array.Copy(result, 0, newResult, 0, result.Length);
                        newResult[newResult.Length - 1] = item;
                        result = newResult;

                    }
                }
            }

            return result;
        }
    }
}
