﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LINQ2Transaction
{
    public class Transaction
    {

        private long transactionId;
        public long TransactionId
        {
            get { return transactionId; }
            set
            {
                transactionId = value;
            }
        }

        private System.DateTime ackDate;
        public System.DateTime AckDate
        {
            get { return ackDate; }
            set
            {
                ackDate = value;
            }
        }

        private decimal depositAmount;
        public decimal DepositAmount
        {
            get { return depositAmount; }
            set
            {
                depositAmount = value;
            }
        }

        private int presenterId;
        public int PresenterId
        {
            get { return presenterId; }
            set
            {
                presenterId = value;
            }
        }

        private int depositAccountId;
        public int DepositAccountId
        {
            get { return depositAccountId; }
            set
            {
                depositAccountId = value;
            }
        }

        private System.DateTime postingDate;
        public System.DateTime PostingDate
        {
            get { return postingDate; }
            set
            {
                postingDate = value;
            }
        }

        private enumTransactionStatus transactionStatus;
        public enumTransactionStatus TransactionStatus
        {
            get { return transactionStatus; }
            set
            {
                transactionStatus = value;
            }
        }

        private enumTransactionType transactionType;
        public enumTransactionType TransactionType
        {
            get { return transactionType; }
            set
            {
                transactionType = value;
            }
        }

        private int locationId;
        public int LocationId
        {
            get { return locationId; }
            set
            {
                locationId = value;
            }
        }

        private System.DateTime processingDate;
        public System.DateTime ProcessingDate
        {
            get { return processingDate; }
            set
            {
                processingDate = value;
            }
        }

        private bool isInBalance = true;
        public bool IsInBalance
        {
            get { return isInBalance; }
            set
            {
                isInBalance = value;
            }
        }

        private int itemCount;
        public int ItemCount
        {
            get { return itemCount; }
            set { itemCount = value; }
        }

        private int workTypeId;
        public int WorkTypeId
        {
            get { return workTypeId; }
            set
            {
                workTypeId = value;
            }
        }

        private enumTransactionType transmitTransactionType;
        public enumTransactionType TransmitTransactionType
        {
            get { return transmitTransactionType; }
            set
            {
                transmitTransactionType = value;
            }
        }

        private string locationName;
        public string LocationName
        {
            get { return locationName; }
            set
            {
                locationName = value;
            }
        }

        private int presentmentId;
        public int PresentmentId
        {
            get { return presentmentId; }
            set
            {
                presentmentId = value;
            }
        }

        private string locationCode;
        public string LocationCode
        {
            get { return locationCode; }
            set
            {
                locationCode = value;
            }
        }

        private int accountType;
        public int AccountType
        {
            get { return accountType; }
            set
            {
                accountType = value;
            }
        }

        private enumTransactionCaptureMethod captureMethod;
        public enumTransactionCaptureMethod CaptureMethod
        {
            get { return captureMethod; }
            set
            {
                captureMethod = value;
            }
        }

        private int checkCount;
        public int CheckCount
        {
            get { return checkCount; }
            set
            {
                checkCount = value;
            }
        }
    }

    //finish enum and add to properites 
    public enum enumTransactionType
    {
        undefined = -1,             //WJS 10/16/08 Support for IB
        payment = 0,
        deposit = 1,
        batch = 2
    }

    public enum enumTransactionStatus
    {
        inProgress = 0,
        released = 1,
        acknowledgedByAcceptance = 2,
        suspended = 3,
        deferred = 4
    }

    public enum enumGetTransactionExAction
    {
        acknowledgedDateRange = 0,
        deferred = 1
    }

    public enum enumUnlockTransactionOptions
    {
        defer = 0,
        release = 1,
        delete = 2,
        doNothing = 3
    }

    public enum enumTransmitTransactionsWantedByLaw
    {
        allTransactions = 0,
        onlyReleased = 1,
        releasedAndRecentlyAck = 2
    }

    public enum enumTransactionCaptureMethod
    {
        desktop = 1, //default for all inserts  // Scanner
        mobile = 2,                              // Mobile
        import = 3                                // Import (aka SimplyDeposit)
    }
}
